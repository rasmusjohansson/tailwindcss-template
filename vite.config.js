const { resolve } = require("path");

export default {
  build: {
    rollupOptions: {
      input: {
        main: resolve(__dirname, "index.html")
        // nested: resolve(__dirname, 'nested/index.html')
      }
    }
  }
};
